import Vue from 'vue'
import VueRouter from 'vue-router'
import my from '../views/my.vue'
import login from '../views/login.vue'
import info from '../views/info.vue'
import infoList from '../views/infoList.vue'


Vue.use(VueRouter)

const routes = [

    {
        path: '/',
        name: 'login',
        component: login
    },
    {
        path: '/my',
        name: 'my',
        component: my
    },
    {
        path: '/info',
        name: 'info',
        component: info
    },
    {
        path: '/infoList',
        name: 'infoList',
        component: infoList
    },
]

//history  hash
const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
