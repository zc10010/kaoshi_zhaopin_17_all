package com.jiyun.controller;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.jiyun.client.RecruitInfoClient;
import com.jiyun.client.UserClient;
import com.jiyun.entity.RecruitDeliver;
import com.jiyun.entity.RecruitInfo;
import com.jiyun.entity.User;
import com.jiyun.entity.UserResume;
import com.jiyun.pojo.MyResult;
import com.jiyun.service.IRecruitDeliverService;
import com.jiyun.service.IUserResumeService;
import com.jiyun.util.email.EmailUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.io.File;
import java.time.LocalDateTime;
import java.util.List;

/**
 * <p>
 *  控制层
 * </p>
 *
 * @author 聪哥哥
 * @since 2023-04-18
 */
@RestController
@RequestMapping("/recruit-deliver")
public class RecruitDeliverController {

    @Autowired
    IRecruitDeliverService recruitDeliverService;


    @Autowired
    IUserResumeService userResumeService;

    @Autowired
    UserClient userClient;

    @Autowired
    RecruitInfoClient recruitInfoClient;

    @Value("${com.jiyun.fileUploadPath}")
    String uploadPath;
    /**
     * 添加接口
     */
    @RequestMapping("/add")
    public MyResult addRecruitDeliver(@RequestBody RecruitDeliver recruitDeliver){
        recruitDeliver.setDeliverTime(LocalDateTime.now());
        recruitDeliverService.save(recruitDeliver);
        //发送邮件

        //标题是xxx投递简历，附件
        /**
         * 1、查出当前人的信息
         * 2、查出简历信息
         * 3、发邮件
         */

        //* 1、查出当前人的信息
        User user = userClient.findById(recruitDeliver.getUid());
        // * 2、查出简历信息
        LambdaQueryWrapper<UserResume> userResumeLambdaQueryWrapper = new LambdaQueryWrapper<>();
        userResumeLambdaQueryWrapper.eq(UserResume::getUid,recruitDeliver.getUid());
        UserResume db = userResumeService.getOne(userResumeLambdaQueryWrapper);

        RecruitInfo recruitInfo = recruitInfoClient.findById(recruitDeliver.getRid());

        EmailUtil.sendEmail(
                "949759888@qq.com",
                user.getUsername()+"求职 "+recruitInfo.getTitle()+" 职位",
                "<div>\n" +
                        "    <h2>\n" +
                        "        来自<strong style=\"color: #3a8ee6;\">"+user.getUsername()+"</strong>的面试申请\n" +
                        "    </h2>\n" +
                        "    <div style=\"margin-left: 163px;\">--积云教育</div>\n" +
                        "</div>",
                new File(uploadPath+db.getWordFileUrl()));



        return MyResult.OK();

    }

    /**
     * 修改接口
     */
    @RequestMapping("/update")
    public void updateRecruitDeliver(@RequestBody RecruitDeliver recruitDeliver){
        recruitDeliverService.updateById(recruitDeliver);
    }

    /**
     * 单个删除接口
     */
    @RequestMapping("/deleteOne")
    public void deleteOne(@RequestParam Integer id){
        recruitDeliverService.removeById(id);
    }

    /**
     * 批量删除接口
     * 前台ajax参数ids传入示例：1,2,3  (英文逗号分隔的字符串)
     */
    @RequestMapping("/deleteBatch")
    public void deleteBatch(@RequestParam List<Integer> ids){
        recruitDeliverService.removeByIds(ids);
    }

    /**
     * 根据id查询实体接口
     */
    @RequestMapping("/findById")
    public RecruitDeliver findById(@RequestParam Integer id){
        return recruitDeliverService.getById(id);
    }

    /**
     * 查询所有接口
     */
    @RequestMapping("/findAll")
    public List<RecruitDeliver> findAll(){
        return recruitDeliverService.list();
    }

    /**
     * 分页查询接口
     * 重要提示：启用分页功能必须在配置类中添加mybatis-plus分页的拦截器
     * 重要提示：启用分页功能必须在配置类中添加mybatis-plus分页的拦截器
     * 重要提示：启用分页功能必须在配置类中添加mybatis-plus分页的拦截器
     */
    @RequestMapping("/findPage")
    public Page<RecruitDeliver> findPage(@RequestParam Integer page,@RequestParam Integer pageSize){
        Page<RecruitDeliver> recruitDeliverPage = new Page<>(page, pageSize);
        return recruitDeliverService.page(recruitDeliverPage);
    }

}
