package com.jiyun.controller;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.jiyun.Word2Pdf;
import com.jiyun.entity.UserResume;
import com.jiyun.pojo.MyResult;
import com.jiyun.service.IUserResumeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.util.List;
import java.util.UUID;

/**
 * <p>
 *  控制层
 * </p>
 *
 * @author 聪哥哥
 * @since 2023-04-18
 */
@RestController
@RequestMapping("/user-resume")
public class UserResumeController {

    @Value("${com.jiyun.fileUploadPath}")
    String uploadPath;

    @Autowired
    IUserResumeService userResumeService;


    @RequestMapping("upload")
    public MyResult upload(MultipartFile file) throws Exception{


        String filename = UUID.randomUUID().toString()+file.getOriginalFilename().substring(file.getOriginalFilename().lastIndexOf("."));

        File file1 = new File(uploadPath + filename);
        file.transferTo(file1);

        //转换pdf
        Word2Pdf.wordToPdf2(file1);


        return MyResult.OK(filename);

    }

    @RequestMapping("findResumeByUid")
    public MyResult findResumeByUid(Integer uid){
        LambdaQueryWrapper<UserResume> userResumeLambdaQueryWrapper = new LambdaQueryWrapper<>();
        userResumeLambdaQueryWrapper.eq(UserResume::getUid,uid);
        return MyResult.OK(userResumeService.getOne(userResumeLambdaQueryWrapper));

    }

    /**
     * 添加接口
     */
    @RequestMapping("/add")
    public MyResult addUserResume(@RequestBody UserResume userResume){

        LambdaQueryWrapper<UserResume> userResumeLambdaQueryWrapper = new LambdaQueryWrapper<>();
        userResumeLambdaQueryWrapper.eq(UserResume::getUid,userResume.getUid());
        UserResume db = userResumeService.getOne(userResumeLambdaQueryWrapper);
        if(db!=null){
            userResume.setId(db.getId());
            userResumeService.updateById(userResume);
        }else{
            userResumeService.save(userResume);
        }
        return MyResult.OK(userResume.getId());
    }

    /**
     * 修改接口
     */
    @RequestMapping("/update")
    public void updateUserResume(@RequestBody UserResume userResume){
        userResumeService.updateById(userResume);
    }

    /**
     * 单个删除接口
     */
    @RequestMapping("/deleteOne")
    public void deleteOne(@RequestParam Integer id){
        userResumeService.removeById(id);
    }

    /**
     * 批量删除接口
     * 前台ajax参数ids传入示例：1,2,3  (英文逗号分隔的字符串)
     */
    @RequestMapping("/deleteBatch")
    public void deleteBatch(@RequestParam List<Integer> ids){
        userResumeService.removeByIds(ids);
    }

    /**
     * 根据id查询实体接口
     */
    @RequestMapping("/findById")
    public UserResume findById(@RequestParam Integer id){
        return userResumeService.getById(id);
    }

    /**
     * 查询所有接口
     */
    @RequestMapping("/findAll")
    public List<UserResume> findAll(){
        return userResumeService.list();
    }

    /**
     * 分页查询接口
     * 重要提示：启用分页功能必须在配置类中添加mybatis-plus分页的拦截器
     * 重要提示：启用分页功能必须在配置类中添加mybatis-plus分页的拦截器
     * 重要提示：启用分页功能必须在配置类中添加mybatis-plus分页的拦截器
     */
    @RequestMapping("/findPage")
    public Page<UserResume> findPage(@RequestParam Integer page,@RequestParam Integer pageSize){
        Page<UserResume> userResumePage = new Page<>(page, pageSize);
        return userResumeService.page(userResumePage);
    }

}
