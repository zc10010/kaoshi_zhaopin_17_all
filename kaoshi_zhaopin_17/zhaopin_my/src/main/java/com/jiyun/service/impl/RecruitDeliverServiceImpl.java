package com.jiyun.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.jiyun.entity.RecruitDeliver;
import com.jiyun.mapper.RecruitDeliverMapper;
import com.jiyun.service.IRecruitDeliverService;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author 聪哥哥
 * @since 2023-04-18
 */
@Service
public class RecruitDeliverServiceImpl extends ServiceImpl<RecruitDeliverMapper, RecruitDeliver> implements IRecruitDeliverService {

}
