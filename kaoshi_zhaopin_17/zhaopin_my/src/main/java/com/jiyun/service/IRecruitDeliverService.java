package com.jiyun.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.jiyun.entity.RecruitDeliver;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author 聪哥哥
 * @since 2023-04-18
 */
public interface IRecruitDeliverService extends IService<RecruitDeliver> {

}
