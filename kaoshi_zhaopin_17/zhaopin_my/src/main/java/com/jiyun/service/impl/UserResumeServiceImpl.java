package com.jiyun.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.jiyun.entity.UserResume;
import com.jiyun.mapper.UserResumeMapper;
import com.jiyun.service.IUserResumeService;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author 聪哥哥
 * @since 2023-04-18
 */
@Service
public class UserResumeServiceImpl extends ServiceImpl<UserResumeMapper, UserResume> implements IUserResumeService {

}
