package com.jiyun.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.jiyun.entity.UserResume;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author 聪哥哥
 * @since 2023-04-18
 */
public interface UserResumeMapper extends BaseMapper<UserResume> {

}
