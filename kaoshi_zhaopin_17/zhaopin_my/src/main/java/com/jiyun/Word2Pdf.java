package com.jiyun;

import com.aspose.words.Document;
import com.aspose.words.License;
import com.aspose.words.SaveFormat;
import org.springframework.core.io.ClassPathResource;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
public class Word2Pdf {

    /**
     * 使用方法是：将上传之后的文件，传给此方法即可
     * 最终生成的文件名为：原文件名+".pdf"
     * @param file
     */
    public static void wordToPdf2(File file) {
        FileOutputStream os = null;
        try {
            //凭证 不然切换后有水印
            InputStream is = new ClassPathResource("/license.xml").getInputStream();
            License aposeLic = new License();
            aposeLic.setLicense(is);
            //生成一个空的PDF文件
            os = new FileOutputStream(new File(file.getAbsolutePath()+".pdf"));
            //要转换的word文件
            Document doc = new Document(file.getAbsolutePath());
            doc.save(os, SaveFormat.PDF);
        } catch (Exception e) {
            e.printStackTrace();
        }finally {
            try {
                os.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
