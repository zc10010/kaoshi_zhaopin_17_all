package com.jiyun.util.email;

import lombok.Data;

import java.io.File;

@Data
public class SenderMessage {

    /**
     * 发件人邮箱
     */
    private String from;

    /**
     * 发件人昵称
     */
    private String personal;

    /**
     * 收件人email
     */
    private String to;

    /**
     * 主题
     */
    private String subject;

    /**
     * 内容
     */
    private String content;

    /**
     * 附件，如需发送多个附件，自行修改为集合
     */
    private File file;

}
