package com.jiyun.util.email;

import lombok.Data;

/**
 * 发送邮件配置
 */
@Data
public class SenderProperties{
    /**
     * 协议
     */
    private String mailTransportProtocol;

    /**
     * smtp授权认证开启，默认开启
     */
    private boolean mailSmtpAuth = true;

    /**
     * 用户名
     */
    private String userName;

    /**
     * 授权码
     */
    private String password;


    /**
     * 邮件服务器地址
     */
    private String mailHost;

    /**
     * 邮件服务器端口号
     */
    private Integer mailPort;

}
