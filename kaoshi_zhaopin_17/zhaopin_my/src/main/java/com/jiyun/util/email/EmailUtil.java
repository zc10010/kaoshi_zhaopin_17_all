package com.jiyun.util.email;

import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.activation.FileDataSource;
import javax.mail.*;
import javax.mail.internet.*;
import java.io.File;
import java.io.UnsupportedEncodingException;
import java.util.Date;
import java.util.Properties;

public class EmailUtil {

    /**
     * 发送者服务器配置
     */
    private static final SenderProperties senderProperties;
    static {
        senderProperties = new SenderProperties();
        senderProperties.setMailSmtpAuth(true);
        senderProperties.setUserName("18514030949@163.com");
        // 这里企业邮箱中，使用登录密码，其他私人邮箱使用客户端授权码
        senderProperties.setPassword("TSFCQNTJEJKPIGBS");
        senderProperties.setMailHost("smtp.163.com");
        senderProperties.setMailTransportProtocol("smtp");
    }

    /**
     * 对外提供的发送邮件方法
     * 返回值为true，发送成功，false发送失败
     * @param to
     * @param subject
     * @param content
     * @return
     */
    public static boolean sendEmail(String to, String subject, String content, File file) {
        SenderMessage senderMessage = new SenderMessage();
        senderMessage.setContent(content);
        senderMessage.setFrom(senderProperties.getUserName());
        senderMessage.setTo(to);
        senderMessage.setSubject(subject);
        senderMessage.setPersonal("积云教育面试平台");
        senderMessage.setFile(file);
        return send(senderMessage);
    }

    public static void main(String[] args) {
        sendEmail("2953787433@qq.com",
                "面试申请",
                "<div>\n" +
                        "    <h2>\n" +
                        "        来自<strong style=\"color: #3a8ee6;\">霍永健</strong>的面试申请\n" +
                        "    </h2>\n" +
                        "    <div style=\"margin-left: 163px;\">--积云教育</div>\n" +
                        "</div>",new File("D:\\allMyFile\\exploreDownload\\Java开发工程师--霍永健.docx"));
    }

    private static boolean send(SenderMessage senderMessage){
        try {
            doSend(senderProperties,senderMessage);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }


    /**
     * 发送邮件
     * @param senderProperties
     */
    private static void doSend(SenderProperties senderProperties, SenderMessage senderMessage) throws MessagingException, UnsupportedEncodingException {

        Properties properties = getProperties(senderProperties);
        Authenticator authenticator = getAuthenticator(senderProperties);
        // 根据properties和authenticator 创建session
        Session session = Session.getDefaultInstance(properties, authenticator);
        Message message = createMessage(session,senderMessage,senderProperties);
        // 发送
        Transport.send(message);
    }
    /**
     * 获取连接信息
     * @param senderProperties
     * @return
     */
    private static Properties getProperties(SenderProperties senderProperties){
        // smtp服务器设置
        Properties props = new Properties();
        // 默认的邮件传输协议
        props.setProperty("mail.transport.protocol", senderProperties.getMailTransportProtocol());
        // 设置邮件服务器主机名
        props.put("mail.host", senderProperties.getMailHost());
        //SMTP 服务器的端口(非SSL连接的端口一般默认为 25, 可以不添加, 如果开启了 SSL 连接,需要改为对应邮箱的 SMTP 服务器的端口, 具体可查看对应邮箱服务的帮助,QQ邮箱的SMTP(SLL)端口为465或587, 其他邮箱自行去查看)
        if (senderProperties.getMailPort() != null){
            props.put("mail.port", senderProperties.getMailPort());
        }
        // 设置是否安全验证,默认为false,一般情况都设置为true
        props.put("mail.smtp.auth", senderProperties.isMailSmtpAuth());
        return props;
    }

    /**
     * 获取授权信息
     * @param senderProperties
     * @return
     */
    private static Authenticator getAuthenticator(SenderProperties senderProperties){
        // 认证信息，即发件人用户名和密码，这里的密码是授权码，不是登录密码，授权码在邮箱设置获取
        return new Authenticator() {
            @Override
            protected PasswordAuthentication getPasswordAuthentication() {
                return new PasswordAuthentication(senderProperties.getUserName(), senderProperties.getPassword());
            }
        };
    }

    /**
     * 创建邮件消息
     * @param session
     * @param senderMessage
     * @param senderProperties
     * @return
     * @throws UnsupportedEncodingException
     * @throws MessagingException
     */
    private static Message createMessage(Session session,SenderMessage senderMessage,SenderProperties senderProperties) throws UnsupportedEncodingException, MessagingException {
        // 根据session创建一个Message，即邮件消息
        Message message = new MimeMessage(session);
        // 设置邮件消息的发送者
        InternetAddress sender = new InternetAddress(senderMessage.getFrom(), senderMessage.getPersonal(), "utf-8");
        message.setFrom(sender);
        // 设置邮件的接受者，如果有多个接受者，调用  message.setRecipients
        InternetAddress receiver = new InternetAddress(senderMessage.getTo());
        message.setRecipient(Message.RecipientType.TO, receiver);
        // 设置主题
        message.setSubject(MimeUtility.encodeText(senderMessage.getSubject()));
        // 发送时间
        message.setSentDate(new Date());

        // 创建多重消息
        Multipart multipart = new MimeMultipart();
        // 创建消息部分
        BodyPart messageBodyPart = new MimeBodyPart();

        // 消息内容（可以使用html标签）
        messageBodyPart.setContent(senderMessage.getContent(), "text/html; charset=UTF-8");

        // 设置文本消息部分
        multipart.addBodyPart(messageBodyPart);

        // 附件部分，如果多附件，循环里面逻辑即可
        if (senderMessage.getFile()!=null){
            messageBodyPart = new MimeBodyPart();
            //把文件，添加到附件1中
            //数据源
            DataSource source = new FileDataSource(senderMessage.getFile());
            //设置第一个附件的数据
            messageBodyPart.setDataHandler(new DataHandler(source));
            //设置附件的文件名
            messageBodyPart.setFileName(MimeUtility.encodeText(senderMessage.getFile().getName()));
            multipart.addBodyPart(messageBodyPart);
        }

        message.setContent(multipart);
        // 保存更改
        message.saveChanges();
        return message;
    }
}
