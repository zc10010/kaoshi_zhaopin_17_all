package com.jiyun.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.jiyun.entity.City;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author 聪哥哥
 * @since 2023-04-18
 */
public interface ICityService extends IService<City> {

}
