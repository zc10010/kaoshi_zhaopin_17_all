package com.jiyun.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.jiyun.entity.Dic;
import com.jiyun.mapper.DicMapper;
import com.jiyun.service.IDicService;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author 聪哥哥
 * @since 2023-04-18
 */
@Service
public class DicServiceImpl extends ServiceImpl<DicMapper, Dic> implements IDicService {

}
