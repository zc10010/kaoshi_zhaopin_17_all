package com.jiyun.controller;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.jiyun.entity.City;
import com.jiyun.pojo.MyResult;
import com.jiyun.service.ICityService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * <p>
 *  控制层
 * </p>
 *
 * @author 聪哥哥
 * @since 2023-04-18
 */
@RestController
@RequestMapping("/city")
public class CityController {

    @Autowired
    ICityService cityService;

    /**
     * 添加接口
     */
    @RequestMapping("/add")
    public void addCity(@RequestBody City city){
        cityService.save(city);
    }

    /**
     * 修改接口
     */
    @RequestMapping("/update")
    public void updateCity(@RequestBody City city){
        cityService.updateById(city);
    }

    /**
     * 单个删除接口
     */
    @RequestMapping("/deleteOne")
    public void deleteOne(@RequestParam Integer id){
        cityService.removeById(id);
    }

    /**
     * 批量删除接口
     * 前台ajax参数ids传入示例：1,2,3  (英文逗号分隔的字符串)
     */
    @RequestMapping("/deleteBatch")
    public void deleteBatch(@RequestParam List<Integer> ids){
        cityService.removeByIds(ids);
    }


    @RequestMapping("findByPid")
    public MyResult findByPid(Integer pid){

        LambdaQueryWrapper<City> cityLambdaQueryWrapper = new LambdaQueryWrapper<>();
        cityLambdaQueryWrapper.eq(City::getPid,pid);
        return MyResult.OK(cityService.list(cityLambdaQueryWrapper));

    }
    /**
     * 根据id查询实体接口
     */
    @RequestMapping("/findById")
    public City findById(@RequestParam Integer id){
        return cityService.getById(id);
    }

    /**
     * 查询所有接口
     */
    @RequestMapping("/findAll")
    public List<City> findAll(){
        return cityService.list();
    }

    /**
     * 分页查询接口
     * 重要提示：启用分页功能必须在配置类中添加mybatis-plus分页的拦截器
     * 重要提示：启用分页功能必须在配置类中添加mybatis-plus分页的拦截器
     * 重要提示：启用分页功能必须在配置类中添加mybatis-plus分页的拦截器
     */
    @RequestMapping("/findPage")
    public Page<City> findPage(@RequestParam Integer page,@RequestParam Integer pageSize){
        Page<City> cityPage = new Page<>(page, pageSize);
        return cityService.page(cityPage);
    }

}
