package com.jiyun.controller;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.jiyun.entity.Dic;
import com.jiyun.pojo.MyResult;
import com.jiyun.service.IDicService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * <p>
 *  控制层
 * </p>
 *
 * @author 聪哥哥
 * @since 2023-04-18
 */
@RestController
@RequestMapping("/dic")
public class DicController {

    @Autowired
    IDicService dicService;

    /**
     * 添加接口
     */
    @RequestMapping("/add")
    public void addDic(@RequestBody Dic dic){
        dicService.save(dic);
    }

    /**
     * 修改接口
     */
    @RequestMapping("/update")
    public void updateDic(@RequestBody Dic dic){
        dicService.updateById(dic);
    }

    /**
     * 单个删除接口
     */
    @RequestMapping("/deleteOne")
    public void deleteOne(@RequestParam Integer id){
        dicService.removeById(id);
    }

    /**
     * 批量删除接口
     * 前台ajax参数ids传入示例：1,2,3  (英文逗号分隔的字符串)
     */
    @RequestMapping("/deleteBatch")
    public void deleteBatch(@RequestParam List<Integer> ids){
        dicService.removeByIds(ids);
    }

    /**
     * 根据id查询实体接口
     */
    @RequestMapping("/findById")
    public Dic findById(@RequestParam Integer id){
        return dicService.getById(id);
    }

    /**
     * 查询所有接口
     */
    @RequestMapping("/findAll")
    public List<Dic> findAll(){
        return dicService.list();
    }

    /**
     * 分页查询接口
     * 重要提示：启用分页功能必须在配置类中添加mybatis-plus分页的拦截器
     * 重要提示：启用分页功能必须在配置类中添加mybatis-plus分页的拦截器
     * 重要提示：启用分页功能必须在配置类中添加mybatis-plus分页的拦截器
     */
    @RequestMapping("/findPage")
    public Page<Dic> findPage(@RequestParam Integer page,@RequestParam Integer pageSize){
        Page<Dic> dicPage = new Page<>(page, pageSize);
        return dicService.page(dicPage);
    }

    @RequestMapping("findByType")
    public MyResult findByType(Integer type){

        LambdaQueryWrapper<Dic> dicLambdaQueryWrapper = new LambdaQueryWrapper<>();
        dicLambdaQueryWrapper.eq(Dic::getType,type);

        return MyResult.OK(dicService.list(dicLambdaQueryWrapper));


    }

}
