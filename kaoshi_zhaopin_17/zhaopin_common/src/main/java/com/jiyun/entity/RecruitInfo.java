package com.jiyun.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDateTime;

/**
 * <p>
 *
 * </p>
 *
 * @author 聪哥哥
 * @since 2023-04-18
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class RecruitInfo implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    private String title;

    private Integer typeId;

    private Integer xueliId;

    private Integer workYears;

    private BigDecimal salary;

    private Integer shengId;

    private Integer shiId;

    private Integer nums;

    private String welfare;

    private String requireInfo;

    private LocalDateTime createTime;

    private Integer createUserId;

    /**
     * 招聘中、停止招聘
     */
    private String state;


}
