package com.jiyun.client;


import com.jiyun.entity.RecruitInfo;
import com.jiyun.entity.User;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

@FeignClient("zhaopininfo")
@RequestMapping("/zhaopininfo/recruit-info")
public interface RecruitInfoClient {

    @RequestMapping("/findById")
    public RecruitInfo findById(@RequestParam("id") Integer id);
}
