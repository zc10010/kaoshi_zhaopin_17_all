package com.jiyun.client;


import com.jiyun.entity.User;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

@FeignClient("zhaopinlogin")
@RequestMapping("/zhaopinlogin/user")
public interface UserClient {

    @RequestMapping("/findById")
    public User findById(@RequestParam("id") Integer id);
}
