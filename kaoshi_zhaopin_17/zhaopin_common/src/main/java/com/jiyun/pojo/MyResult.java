package com.jiyun.pojo;

import lombok.Data;

import java.io.Serializable;

@Data
public class MyResult implements Serializable {

    private Integer code;//state,请求状态，1成功；2失败
    private String message;//请求信息，
    private Object data;



    //增删改的时候
    public static MyResult OK(){
        MyResult myResult = new MyResult();
        myResult.setCode(1);
        myResult.setMessage("请求成功");
        return myResult;
    }

    public static MyResult OK(Object data){
        MyResult myResult = new MyResult();
        myResult.setCode(1);
        myResult.setMessage("请求成功");
        myResult.setData(data);
        return myResult;
    }
    public static MyResult ERROR(String message){
        MyResult myResult = new MyResult();
        myResult.setCode(2);
        myResult.setMessage(message);
        return myResult;
    }

    public static MyResult ERROR_code3(String message){
        MyResult myResult = new MyResult();
        myResult.setCode(3);
        myResult.setMessage(message);
        return myResult;
    }

}
