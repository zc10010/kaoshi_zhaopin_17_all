package com.jiyun.dto;

import com.jiyun.entity.User;
import lombok.Data;

import java.io.Serializable;

@Data
public class UserLoginDto extends User implements Serializable {


    private String code;
    private Boolean isHr;


}
