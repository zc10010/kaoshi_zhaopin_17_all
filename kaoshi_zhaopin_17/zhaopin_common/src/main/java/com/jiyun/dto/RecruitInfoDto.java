package com.jiyun.dto;

import com.jiyun.entity.RecruitInfo;
import lombok.Data;

import java.io.Serializable;
import java.time.LocalDateTime;

@Data
public class RecruitInfoDto extends RecruitInfo implements Serializable {

    //响应参数
    private String typeName;
    private String xueliName;
    private String shengName;
    private String shiName;


    //查询参数
    private Integer timeType;
    private LocalDateTime start;
    private LocalDateTime end;



}
