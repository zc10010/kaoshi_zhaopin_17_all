package com.jiyun.service.impl;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.jiyun.dto.RecruitInfoDto;
import com.jiyun.entity.RecruitInfo;
import com.jiyun.mapper.RecruitInfoMapper;
import com.jiyun.service.IRecruitInfoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author 聪哥哥
 * @since 2023-04-18
 */
@Service
public class RecruitInfoServiceImpl extends ServiceImpl<RecruitInfoMapper, RecruitInfo> implements IRecruitInfoService {

    @Autowired
    RecruitInfoMapper recruitInfoMapper;


    @Override
    public Page<RecruitInfoDto> findPage(Integer page, Integer pageSize, RecruitInfoDto recruitInfoDto) {

        Page<RecruitInfoDto> recruitInfoDtoPage = new Page<>(page, pageSize);
        recruitInfoMapper.findPage(recruitInfoDtoPage,recruitInfoDto);

        return recruitInfoDtoPage;
    }
}
