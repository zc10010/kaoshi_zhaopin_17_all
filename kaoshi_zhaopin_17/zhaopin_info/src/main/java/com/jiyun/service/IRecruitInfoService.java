package com.jiyun.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import com.jiyun.dto.RecruitInfoDto;
import com.jiyun.entity.RecruitInfo;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author 聪哥哥
 * @since 2023-04-18
 */
public interface IRecruitInfoService extends IService<RecruitInfo> {

    Page<RecruitInfoDto> findPage(Integer page, Integer pageSize, RecruitInfoDto recruitInfoDto);
}
