package com.jiyun.controller;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.jiyun.dto.RecruitInfoDto;
import com.jiyun.entity.RecruitInfo;
import com.jiyun.pojo.MyResult;
import com.jiyun.service.IRecruitInfoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.time.LocalDateTime;
import java.util.List;

/**
 * <p>
 *  控制层
 * </p>
 *
 * @author 聪哥哥
 * @since 2023-04-18
 */
@RestController
@RequestMapping("/recruit-info")
public class RecruitInfoController {

    @Autowired
    IRecruitInfoService recruitInfoService;

    /**
     * 添加接口
     */
    @RequestMapping("/add")
    public void addRecruitInfo(@RequestBody RecruitInfo recruitInfo){
        recruitInfo.setCreateTime(LocalDateTime.now());
        recruitInfoService.save(recruitInfo);
    }

    /**
     * 修改接口
     */
    @RequestMapping("/update")
    public void updateRecruitInfo(@RequestBody RecruitInfo recruitInfo){
        recruitInfoService.updateById(recruitInfo);
    }

    /**
     * 单个删除接口
     */
    @RequestMapping("/deleteOne")
    public void deleteOne(@RequestParam Integer id){
        recruitInfoService.removeById(id);
    }

    /**
     * 批量删除接口
     * 前台ajax参数ids传入示例：1,2,3  (英文逗号分隔的字符串)
     */
    @RequestMapping("/deleteBatch")
    public void deleteBatch(@RequestParam List<Integer> ids){
        recruitInfoService.removeByIds(ids);
    }

    /**
     * 根据id查询实体接口
     */
    @RequestMapping("/findById")
    public RecruitInfo findById(@RequestParam Integer id){
        return recruitInfoService.getById(id);
    }

    /**
     * 查询所有接口
     */
    @RequestMapping("/findAll")
    public List<RecruitInfo> findAll(){
        return recruitInfoService.list();
    }

    /**
     * 分页查询接口
     * 重要提示：启用分页功能必须在配置类中添加mybatis-plus分页的拦截器
     * 重要提示：启用分页功能必须在配置类中添加mybatis-plus分页的拦截器
     * 重要提示：启用分页功能必须在配置类中添加mybatis-plus分页的拦截器
     */
    @RequestMapping("/findPage")
    public MyResult findPage(@RequestParam Integer page, @RequestParam Integer pageSize, @RequestBody RecruitInfoDto recruitInfoDto){


        /**
         * 1、info库中表中
         * 2、省市信息学历dic表中
         */

        if (recruitInfoDto.getTimeType()!=null) {
            recruitInfoDto.setEnd(LocalDateTime.now());//
            switch (recruitInfoDto.getTimeType()){
                case 1:
                    recruitInfoDto.setStart(LocalDateTime.now().minusDays(2));
                    break;
                case 2:
                    recruitInfoDto.setStart(LocalDateTime.now().minusDays(6));
                    break;
                case 3:
                    recruitInfoDto.setStart(LocalDateTime.now().minusDays(14));
                    break;
                case 4:
                    recruitInfoDto.setStart(LocalDateTime.now().minusDays(29));
                    break;
            }

        }

        Page<RecruitInfoDto> recruitInfoPage = recruitInfoService.findPage(page,pageSize,recruitInfoDto);


        return MyResult.OK(recruitInfoPage);
    }

}
