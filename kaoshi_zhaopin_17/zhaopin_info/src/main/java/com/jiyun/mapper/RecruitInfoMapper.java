package com.jiyun.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.jiyun.dto.RecruitInfoDto;
import com.jiyun.entity.RecruitInfo;
import org.apache.ibatis.annotations.Param;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author 聪哥哥
 * @since 2023-04-18
 */
public interface RecruitInfoMapper extends BaseMapper<RecruitInfo> {

    Page<RecruitInfoDto> findPage(Page<RecruitInfoDto> recruitInfoDtoPage, @Param("dto") RecruitInfoDto recruitInfoDto);
}
