/*
Navicat MySQL Data Transfer

Source Server         : localhost
Source Server Version : 50561
Source Host           : localhost:3306
Source Database       : 17_kaoshi_zhaopin_info

Target Server Type    : MYSQL
Target Server Version : 50561
File Encoding         : 65001

Date: 2023-04-20 14:46:44
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for recruit_info
-- ----------------------------
DROP TABLE IF EXISTS `recruit_info`;
CREATE TABLE `recruit_info` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) DEFAULT NULL,
  `type_id` int(11) DEFAULT NULL,
  `xueli_id` int(11) DEFAULT NULL,
  `work_years` int(8) DEFAULT NULL,
  `salary` decimal(10,2) DEFAULT NULL,
  `sheng_id` int(11) DEFAULT NULL,
  `shi_id` int(11) DEFAULT NULL,
  `nums` int(7) DEFAULT NULL,
  `welfare` varchar(255) DEFAULT NULL,
  `require_info` varchar(255) DEFAULT NULL,
  `create_time` datetime DEFAULT NULL,
  `create_user_id` int(8) DEFAULT NULL,
  `state` varchar(255) DEFAULT NULL COMMENT '招聘中、停止招聘',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of recruit_info
-- ----------------------------
INSERT INTO `recruit_info` VALUES ('1', 'JAVA高级工程师', '1', '7', '3', '28000.00', '1', '2', '10', '996,18薪', '要求很多很多很多很多很多很多很多很多很多很多很多很多', '2023-04-12 16:57:54', '1', '招聘中');
INSERT INTO `recruit_info` VALUES ('2', 'JAVA架构师', '1', '5', '10', '50000.00', '1', '2', '1', '007,全年无休', '也是很多\n很多\n很多', '2023-04-19 11:00:39', '1', '招聘中');
