/*
Navicat MySQL Data Transfer

Source Server         : localhost
Source Server Version : 50561
Source Host           : localhost:3306
Source Database       : 17_kaoshi_zhaopin_login

Target Server Type    : MYSQL
Target Server Version : 50561
File Encoding         : 65001

Date: 2023-04-20 14:46:50
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for user
-- ----------------------------
DROP TABLE IF EXISTS `user`;
CREATE TABLE `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(255) DEFAULT NULL,
  `phone` varchar(255) DEFAULT NULL,
  `compony` varchar(255) DEFAULT NULL,
  `type` varchar(255) DEFAULT NULL COMMENT '1hr；2求职者',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of user
-- ----------------------------
INSERT INTO `user` VALUES ('1', '1', '1', '郑州优思安', '1');
INSERT INTO `user` VALUES ('2', '2', '2', '阿里巴巴', '1');
INSERT INTO `user` VALUES ('3', '张三', '3', '', '2');
INSERT INTO `user` VALUES ('4', '李四', '4', '', '2');
