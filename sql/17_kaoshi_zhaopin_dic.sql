/*
Navicat MySQL Data Transfer

Source Server         : localhost
Source Server Version : 50561
Source Host           : localhost:3306
Source Database       : 17_kaoshi_zhaopin_dic

Target Server Type    : MYSQL
Target Server Version : 50561
File Encoding         : 65001

Date: 2023-04-20 14:46:38
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for city
-- ----------------------------
DROP TABLE IF EXISTS `city`;
CREATE TABLE `city` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `pid` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of city
-- ----------------------------
INSERT INTO `city` VALUES ('1', '河南', '0');
INSERT INTO `city` VALUES ('2', '郑州', '1');
INSERT INTO `city` VALUES ('3', '驻马店', '1');
INSERT INTO `city` VALUES ('4', '平顶山', '1');
INSERT INTO `city` VALUES ('5', '河北', '0');
INSERT INTO `city` VALUES ('6', '邯郸', '5');
INSERT INTO `city` VALUES ('7', '廊坊', '5');

-- ----------------------------
-- Table structure for dic
-- ----------------------------
DROP TABLE IF EXISTS `dic`;
CREATE TABLE `dic` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL COMMENT '1代表职位类型；2代表学历；',
  `type` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of dic
-- ----------------------------
INSERT INTO `dic` VALUES ('1', 'JAVA', '1');
INSERT INTO `dic` VALUES ('2', '前端', '1');
INSERT INTO `dic` VALUES ('3', '运维', '1');
INSERT INTO `dic` VALUES ('4', '测试', '1');
INSERT INTO `dic` VALUES ('5', '大专', '2');
INSERT INTO `dic` VALUES ('6', '本科', '2');
INSERT INTO `dic` VALUES ('7', '硕士', '2');
INSERT INTO `dic` VALUES ('8', '博士', '2');
