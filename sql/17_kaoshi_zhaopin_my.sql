/*
Navicat MySQL Data Transfer

Source Server         : localhost
Source Server Version : 50561
Source Host           : localhost:3306
Source Database       : 17_kaoshi_zhaopin_my

Target Server Type    : MYSQL
Target Server Version : 50561
File Encoding         : 65001

Date: 2023-04-20 14:46:56
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for recruit_deliver
-- ----------------------------
DROP TABLE IF EXISTS `recruit_deliver`;
CREATE TABLE `recruit_deliver` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uid` int(11) DEFAULT NULL,
  `rid` int(11) DEFAULT NULL,
  `deliver_time` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of recruit_deliver
-- ----------------------------
INSERT INTO `recruit_deliver` VALUES ('1', '3', '2', '2023-04-19 17:19:43');

-- ----------------------------
-- Table structure for user_resume
-- ----------------------------
DROP TABLE IF EXISTS `user_resume`;
CREATE TABLE `user_resume` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uid` int(11) DEFAULT NULL,
  `word_file_url` varchar(255) DEFAULT NULL,
  `priview_file_url` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of user_resume
-- ----------------------------
INSERT INTO `user_resume` VALUES ('1', '3', '37194e6a-60e8-49f1-b28b-1b893b74511d.docx', '37194e6a-60e8-49f1-b28b-1b893b74511d.docx.pdf');
